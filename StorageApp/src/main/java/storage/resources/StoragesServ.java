package storage.resources;

import com.google.gson.Gson;
import storage.controller.StorageController;
import storage.controller.StorageControllerImpl;
import storage.data.Storage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class StoragesServ extends HttpServlet {
    private final ParameterValidation parameterValidationInstance = ParameterValidation.getInstance();
    private final StorageController storageControllerInstance = StorageControllerImpl.getInstance();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Storage> storagesList = storageControllerInstance.getAllStorages();

        String json = new Gson().toJson(storagesList);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.printf(json);
        out.flush();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Integer> ids = new ArrayList<>();
        for (Storage storage : storageControllerInstance.getAllStorages()) {
            ids.add(storage.getId());
        }
//        ids = controller.getAllStorages().stream()
//                .map(storage -> storage.getId())
//                .collect(Collectors.toList());

        storageControllerInstance.deleteStorages(ids);

        parameterValidationInstance
                .printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
    }
}
