package storage.resources;

import com.google.gson.Gson;
import storage.controller.ItemsController;
import storage.controller.ItemsControllerImpl;
import storage.data.ItemsInStorage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.DateTimeException;
import java.time.LocalDate;

public class ItemServ extends HttpServlet {
    private final static String ITEM_ID = "id";
    private final static String ITEM_NAME = "name";
    private final static String COMPARTMENT_ID = "compartmentId";
    private final static String DATE_ADDED = "dateAdded";
    private final static String EXPIRY_DATE = "expiryDate";
    private final static String QUANTITY = "quantity";

    ParameterValidation paramValidationInstance = ParameterValidation.getInstance();
    ItemsController itemsController = ItemsControllerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ItemsInStorage item;
        try {
            item = getValidatedExistingItem(req);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (item == null) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
            return;
        }

        String json = new Gson().toJson(item);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.print(json);
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ItemsInStorage item;
        try {
            item = getValidatedNewItem(req);
        } catch (IllegalArgumentException | DateTimeException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (itemsController.addItemInStorage(item)) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ItemsInStorage item;
        try {
            item = getValidatedModifiedItem(req);
        } catch (IllegalArgumentException | DateTimeException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (itemsController.modifyItemInStorage(item)) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id;
        try {
            id = paramValidationInstance.getNonNullOrThrow(req, ITEM_ID);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        int parsedInt;
        try {
            parsedInt = Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + ITEM_ID);
            return;
        }

        if (itemsController.deleteItemInStorage(parsedInt)) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    private ItemsInStorage getValidatedExistingItem(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String id = paramValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);

        int parsedId;
        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }

        return itemsController.getItemInStorage(parsedId);
    }

    private ItemsInStorage getValidatedNewItem(HttpServletRequest req) throws IllegalArgumentException,DateTimeException, IOException {
        String name = paramValidationInstance.getNonNullOrThrow(req, ITEM_NAME);
        String dateAdded = paramValidationInstance.getNonNullOrThrow(req, DATE_ADDED);
        String expiryDate = paramValidationInstance.getNonNullOrThrow(req, EXPIRY_DATE);
        String quantity = paramValidationInstance.getNonNullOrThrow(req, QUANTITY);
        String compartmentId = paramValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);

        ItemsInStorage item;
        try {
            item = new ItemsInStorage.Builder()
                    .setName(name)
                    .setQuantity(Integer.parseInt(quantity))
                    .setCompartmentId(Integer.parseInt(compartmentId))
                    .setDateAdded(LocalDate.parse(dateAdded))
                    .setDateOfExpiration(LocalDate.parse(expiryDate))
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        } catch (DateTimeException dte) {
            throw new DateTimeException(ParameterValidation.NOT_LOCAL_DATE_ERROR);
        }

        return item;
    }

    private ItemsInStorage getValidatedModifiedItem(HttpServletRequest req) throws IllegalArgumentException, DateTimeException, IOException {
        String id = paramValidationInstance.getNonNullOrThrow(req, ITEM_ID);
        String name = paramValidationInstance.getNonNullOrThrow(req, ITEM_NAME);
        String dateAdded = paramValidationInstance.getNonNullOrThrow(req, DATE_ADDED);
        String expiryDate = paramValidationInstance.getNonNullOrThrow(req, EXPIRY_DATE);
        String quantity = paramValidationInstance.getNonNullOrThrow(req, QUANTITY);
        String compartmentId = paramValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);

        ItemsInStorage item;
        try {
            item = new ItemsInStorage.Builder()
                    .setName(name)
                    .setId(Integer.parseInt(id))
                    .setQuantity(Integer.parseInt(quantity))
                    .setCompartmentId(Integer.parseInt(compartmentId))
                    .setDateAdded(LocalDate.parse(dateAdded))
                    .setDateOfExpiration(LocalDate.parse(expiryDate))
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        } catch (DateTimeException e) {
            throw new DateTimeException(ParameterValidation.NOT_LOCAL_DATE_ERROR);
        }
        return item;
    }


}
