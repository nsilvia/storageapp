package storage.resources;

import com.google.gson.Gson;
import storage.controller.CompartmentsControllerImpl;
import storage.controller.StorageControllerImpl;
import storage.data.Compartment;
import storage.data.Storage;
import storage.dto.StorageDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class StorageServ extends HttpServlet {
    private static final String STORAGE_NAME = "name";
    private static final String STORAGE_ID = "id";

    private final StorageControllerImpl storageControllerInstance = StorageControllerImpl.getInstance();
    private final CompartmentsControllerImpl compartmentsControllerInstance = CompartmentsControllerImpl.getInstance();
    private final ParameterValidation paramValidationInstance = ParameterValidation.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Storage storage;
        try {
            storage = getValidatedExistingStorage(req);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (storage == null) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
            return;
        }
        List<Compartment> compartments = compartmentsControllerInstance.getAllCompartmentsInStorage(storage.getId());

        StorageDTO storageDTO = new StorageDTO();
        storageDTO.setId(storage.getId());
        storageDTO.setStorageName(storage.getStorageName());
        storageDTO.setCompartmentList(compartments);

        String json = new Gson().toJson(storageDTO);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.print(json);
        out.flush();

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Storage storage;
        try {
            storage = getValidatedNewStorage(req);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (storageControllerInstance.addStorage(storage)) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Storage storage;
        try {
            storage = getValidatedModifiedStorage(req);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (storageControllerInstance.modifyStorage(storage)) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id;

        try {
            id = paramValidationInstance.getNonNullOrThrow(req, STORAGE_ID);
        } catch (IllegalArgumentException iae) {
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        int parsedInt;
        try{
            parsedInt = Integer.parseInt(id);
        } catch (NumberFormatException nfe){
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, nfe.getMessage());
            return;
        }
        if (storageControllerInstance.deleteStorage(parsedInt)){
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else{
            paramValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    private Storage getValidatedModifiedStorage(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String id = paramValidationInstance.getNonNullOrThrow(req, STORAGE_ID);
        String name = paramValidationInstance.getNonNullOrThrow(req, STORAGE_NAME);

        Storage storage;

        try {
            storage = new Storage.Builder()
                    .setId(Integer.parseInt(id))
                    .setStorageName(name)
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }
        return storage;
    }

    private Storage getValidatedNewStorage(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String name = paramValidationInstance.getNonNullOrThrow(req, STORAGE_NAME);

        Storage storage;

        try {
            storage = new Storage.Builder()
                    .setStorageName(name)
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }
        return storage;
    }

    private Storage getValidatedExistingStorage(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String id = paramValidationInstance.getNonNullOrThrow(req, STORAGE_ID);

        Storage storage;
        try {
            storage = storageControllerInstance.getStorage(Integer.parseInt(id));
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }
        return storage;
    }

}
