package storage.resources;

import com.google.gson.Gson;
import storage.controller.CompartmentsController;
import storage.controller.CompartmentsControllerImpl;
import storage.controller.StorageController;
import storage.controller.StorageControllerImpl;
import storage.data.Compartment;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CompartmentsServ extends HttpServlet {
    private static final String STORAGE_ID = "storageId";

    private final ParameterValidation parameterValidationInstance = ParameterValidation.getInstance();
    private final CompartmentsController compartmentsControllerInstance = CompartmentsControllerImpl.getInstance();
    private final StorageController storageControllerInstance = StorageControllerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter(STORAGE_ID);

        int parsedId;
        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + STORAGE_ID);
            return;
        }

        if (storageControllerInstance.getStorage(parsedId) == null) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
        }

        List<Compartment> compartmentsList = compartmentsControllerInstance.getAllCompartmentsInStorage(parsedId);

        String json = new Gson().toJson(compartmentsList);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.printf(json);
        out.flush();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter(STORAGE_ID);

        int parsedId;
        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + STORAGE_ID);
            return;
        }

        if (storageControllerInstance.getStorage(parsedId) == null) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
            return;
        }

        List<Integer> ids = new ArrayList<>();
        for (Compartment compartment : compartmentsControllerInstance.getAllCompartmentsInStorage(parsedId)) {
            ids.add(compartment.getId());
        }

        compartmentsControllerInstance.deleteCompartments(ids);
        parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
    }
}
