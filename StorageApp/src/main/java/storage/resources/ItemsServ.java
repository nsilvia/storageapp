package storage.resources;

import com.google.gson.Gson;
import storage.controller.CompartmentsController;
import storage.controller.CompartmentsControllerImpl;
import storage.controller.ItemsController;
import storage.controller.ItemsControllerImpl;
import storage.data.ItemsInStorage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ItemsServ extends HttpServlet {
    private static final String COMPARTMENT_ID = "compartmentId";

    private final ItemsController itemsControllerInstance = ItemsControllerImpl.getInstance();
    private final CompartmentsController compartmentControllerInstance = CompartmentsControllerImpl.getInstance();
    private final ParameterValidation parameterValidationInstance = ParameterValidation.getInstance();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String compartmentID = req.getParameter(COMPARTMENT_ID);

        int parsedId;
        try {
            parsedId = Integer.parseInt(compartmentID);
        } catch (NumberFormatException nfe) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + COMPARTMENT_ID);
            return;
        }

        if (compartmentControllerInstance.getCompartment(parsedId) == null) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
            return;
        }

        List<ItemsInStorage> itemsList = itemsControllerInstance.getAllItemsInStorage(parsedId);

        String json = new Gson().toJson(itemsList);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.printf(json);
        out.flush();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter(COMPARTMENT_ID);

        int parsedId;
        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + COMPARTMENT_ID);
            return;
        }

        if (compartmentControllerInstance.getCompartment(parsedId) == null) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
        }

        List<Integer> ids = new ArrayList<>();
        for(ItemsInStorage item : itemsControllerInstance.getAllItemsInStorage(parsedId)){
            ids.add(item.getId());
        }

        itemsControllerInstance.deleteItemsInStorage(ids);
        parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
    }
}
