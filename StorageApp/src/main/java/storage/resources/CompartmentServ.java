package storage.resources;

import com.google.gson.Gson;
import storage.controller.CompartmentsControllerImpl;
import storage.controller.ItemsControllerImpl;
import storage.data.Compartment;
import storage.data.ItemsInStorage;
import storage.dto.CompartmentDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CompartmentServ extends HttpServlet {
    private final static String COMPARTMENT_NAME = "name";
    private final static String COMPARTMENT_POSITION = "position";
    private final static String STORAGE_ID = "storageId";
    private final static String COMPARTMENT_ID = "id";

    private final CompartmentsControllerImpl compartmentControllerInstance = CompartmentsControllerImpl.getInstance();
    private final ParameterValidation parameterValidationInstance = ParameterValidation.getInstance();
    private final ItemsControllerImpl itemsControllerInstance = ItemsControllerImpl.getInstance();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Compartment compartment;
        try {
            compartment = getValidatedExistingCompartment(req);
        } catch (IllegalArgumentException iae) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (compartment == null) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ITEM_NOT_FOUND);
            return;
        }

        List<ItemsInStorage> itemsList = itemsControllerInstance.getAllItemsInStorage(compartment.getId());
        CompartmentDTO compartmentDTO = new CompartmentDTO();
        compartmentDTO.setId(compartment.getId());
        compartmentDTO.setName(compartment.getCompartmentName());
        compartmentDTO.setPosition(compartment.getPosition());
        compartmentDTO.setStorageId(compartment.getStorageId());
        compartmentDTO.setListOfItems(itemsList);

        String json = new Gson().toJson(compartmentDTO);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.print(json);
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Compartment compartment;
        try {
            compartment = getValidatedNewCompartment(req);
        } catch (IllegalArgumentException iae) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (compartmentControllerInstance.addCompartment(compartment)) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Compartment compartment;
        try {
            compartment = getValidatedModifiedCompartment(req);
        } catch (IllegalArgumentException iae) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        if (compartmentControllerInstance.modifyCompartment(compartment)) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id;
        try {
            id = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);
        } catch (IllegalArgumentException iae) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, iae.getMessage());
            return;
        }

        int parsedId;
        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.NOT_INTEGER_ERROR + COMPARTMENT_ID);
            return;
        }

        if (compartmentControllerInstance.deleteCompartment(parsedId)) {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_OK, ParameterValidation.SUCCESSFUL_MESSAGE);
        } else {
            parameterValidationInstance.printMessage(resp, HttpServletResponse.SC_BAD_REQUEST, ParameterValidation.ACTION_FAILED_ERROR);
        }
    }

    private Compartment getValidatedExistingCompartment(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String id = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);

        int parsedId;
        try{
            parsedId = Integer.parseInt(id);
        }  catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }

       return compartmentControllerInstance.getCompartment(parsedId);

    }

    private Compartment getValidatedNewCompartment(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String name = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_NAME);
        String position = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_POSITION);
        String storageId = parameterValidationInstance.getNonNullOrThrow(req, STORAGE_ID);

        Compartment compartment;

        try {
            compartment = new Compartment.Builder()
                    .setCompartmentName(name)
                    .setPosition(position)
                    .setStorageId(Integer.parseInt(storageId))
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }
        return compartment;
    }

    private Compartment getValidatedModifiedCompartment(HttpServletRequest req) throws IllegalArgumentException, IOException {
        String id = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_ID);
        String name = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_NAME);
        String position = parameterValidationInstance.getNonNullOrThrow(req, COMPARTMENT_POSITION);
        String storageId = parameterValidationInstance.getNonNullOrThrow(req, STORAGE_ID);

        Compartment compartment;

        try {
            compartment = new Compartment.Builder()
                    .setId(Integer.parseInt(id))
                    .setCompartmentName(name)
                    .setPosition(position)
                    .setStorageId(Integer.parseInt(storageId))
                    .build();
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ParameterValidation.NOT_INTEGER_ERROR);
        }
        return compartment;
    }


}

