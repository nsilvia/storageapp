package storage.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ParameterValidation {
    static final String NOT_NULL_ERROR = "Error! The following parameter cannot be null - ";
    static final String NOT_INTEGER_ERROR = "Invalid parameter! ID must be integer!";
    static final String NOT_LOCAL_DATE_ERROR = "Invalid format! Date must follow the patter: 2007-12-03!";
    static final String ACTION_FAILED_ERROR = "Action failed! Please try again!";
    static final String ITEM_NOT_FOUND = "No such element found. Invalid ID.";

    static final String SUCCESSFUL_MESSAGE = "Action completed successful!";

    static ParameterValidation instance = new ParameterValidation();

    private ParameterValidation(){}

    public static ParameterValidation getInstance(){
        return instance;
    }

    void printMessage(HttpServletResponse resp, int status, String message) throws IOException {
        resp.setStatus(status);
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        out.print(message);
        out.flush();
    }

    String getNonNullOrThrow(HttpServletRequest req, String parameterName) throws IllegalArgumentException{
        String userInput = req.getParameter(parameterName);
        if(userInput == null){
            throw new IllegalArgumentException(String.format("%s %s", NOT_NULL_ERROR, parameterName));
        }
        return userInput;
    }
}
