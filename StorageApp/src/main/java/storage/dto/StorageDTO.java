package storage.dto;

import storage.data.Compartment;

import java.util.List;

public class StorageDTO {
    private int id;
    private String storageName;
    private List<Compartment> compartmentList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public List<Compartment> getCompartmentList() {
        return compartmentList;
    }

    public void setCompartmentList(List<Compartment> compartmentList) {
        this.compartmentList = compartmentList;
    }
}
