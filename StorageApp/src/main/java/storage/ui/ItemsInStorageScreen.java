package storage.ui;

import storage.controller.ItemsControllerImpl;
import storage.data.ItemsInStorage;
import storage.ui.controller.Result.Result;
import storage.ui.controller.Result.UiAction;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class ItemsInStorageScreen extends AbstractScreen {
    private static final String COMMAND_MESSAGE = "Type here your command: ";
    private static final String ITEM_NAME_COMMAND = "Set Item name: ";
    private static final String ITEM_IN_STORAGE_ID_COMMAND = "Select Item ID: ";
    private static final String DATE_ADDED_COMMAND = String.format("Set Date when added: %nformat-> 31/12/1900");
    private static final String EXPIRY_DATE_COMMAND = String.format("Set Expiry date: %nformat-> 31/12/1900");
    private static final String QUANTITY_COMMAND = "Set Quantity: ";

    private Map<Integer, ItemsInStorage> itemsInStorageMap = new HashMap<>();

    private final ItemsControllerImpl itemsInStorageControllerInstance = ItemsControllerImpl.getInstance();
    private final int storageId;
    private final int compartmentId;
    private final String compartmentName;

    public ItemsInStorageScreen(int storageId, int compartmentId, String compartmentName) {
        this.storageId = storageId;
        this.compartmentId = compartmentId;
        this.compartmentName = compartmentName;
    }

    @Override
    public void displayGreetingMessage() {
        System.out.printf("%s %s %s%n", SCREEN_SEPARATOR, compartmentName, SCREEN_SEPARATOR);
    }

    @Override
    public void displayScreenDetails() {
        System.out.printf("%s%n%s", displayNumberOfItems(), getNamesAndIdsOfItems(compartmentId));
    }

    @Override
    public void displayLegendInstructions() {
        String format = "%1$-29s%2$-1s%n";
        System.out.println(LEGEND_SEPARATOR);
        System.out.printf(format, "ACTION", "COMMAND");
        System.out.printf(format, SELECT_MESSAGE, SELECT_CODE);
        System.out.printf(format, ADD_MESSAGE, ADD_CODE);
        System.out.printf(format, MODIFY_MESSAGE, MODIFY_CODE);
        System.out.printf(format, DELETE_MESSAGE, DELETE_CODE);
        System.out.printf(format, DELETE_ALL_MESSAGE, DELETE_ALL_CODE);
        System.out.printf(format, BACK_COMMAND, BACK_CODE);
        System.out.printf(format, CLOSE_APP_MESSAGE, CLOSE_APP_CODE);
        System.out.println(LEGEND_SEPARATOR);
    }

    @Override
    public String expectUserCommand() {
        System.out.println(COMMAND_MESSAGE);
        return requestUserInput();
    }

    @Override
    public String displayUserRelevantDetails(int itemId) {
        ItemsInStorage currentItem = itemsInStorageControllerInstance.getItemInStorage(itemId);
        return String.format("name = %1$s, quantity = %2$s, dateAdded = %3$td %3$tm %3$tY, dateOfExpiration = %4$td %4$tm %4$tY%n",
                currentItem.getItemInStorageName(), currentItem.getQuantity(), currentItem.getDateAdded(), currentItem.getDateOfExpiration());
    }

    @Override
    public Result runUserCommand(String userInputCodeCommand) {
        switch (userInputCodeCommand) {
            case BACK_CODE:
                return Result.successful(UiAction.SHOW_SCREEN, "", new CompartmentsScreen(storageId));
            case ADD_CODE:
                return addItemInStorage();
            case MODIFY_CODE:
                return modifyItemInStorage();
            case DELETE_CODE:
                return deleteItemInStorage();
            case DELETE_ALL_CODE:
                return deleteAllItemsInStorage();
            case SELECT_CODE:
                return selectItem();
            case CLOSE_APP_CODE:
                return Result.successful(UiAction.EXIT, "Goodbye!", this);
            default:
                return Result.failed(UiAction.SHOW_SCREEN, INVALID_GENERAL_INPUT, this);
        }
    }

    LocalDate getAddedDateFromUser() {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println(DATE_ADDED_COMMAND);
        String userInput = requestUserInput();
        try {
            return LocalDate.parse(userInput, f);
        } catch (DateTimeParseException dtpe) {
            System.out.println("Invalid input - Follow the pattern 31 12 1900;");
            getAddedDateFromUser();
        }
        return null;
    }

    LocalDate getExpiryDateFromUser() {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println(EXPIRY_DATE_COMMAND);
        String userInput = requestUserInput();
        try {
            return LocalDate.parse(userInput, f);
        } catch (DateTimeParseException dtpe) {
            System.out.println("Invalid input - Follow the pattern 31 12 1900;");
            getExpiryDateFromUser();
        }
        return null;

    }

    int getQuantityFromUser() {
        System.out.println(QUANTITY_COMMAND);
        return requestIntUserInput();
    }

    String getNameFromUser() {
        System.out.println(ITEM_NAME_COMMAND);
        return requestUserInput();
    }

    private String getNamesAndIdsOfItems(int compartmentId) {
        List<ItemsInStorage> itemsInStorage = itemsInStorageControllerInstance.getAllItemsInStorage(compartmentId);
        StringBuilder messageWithAllItems = new StringBuilder();
        int generatedItemId = 0;

        for (ItemsInStorage item : itemsInStorage) {
            generatedItemId++;
            itemsInStorageMap.put(generatedItemId, item);
            messageWithAllItems.append(
                    String.format("Item id: %d; Item name: %s;%n", generatedItemId,  item.getItemInStorageName())
            );
        }
        return messageWithAllItems.toString();
    }

    private String displayNumberOfItems() {
        return String.format("The selected compartment has %d items:%n",
                itemsInStorageControllerInstance.getAllItemsInStorage(compartmentId).size());
    }

    private int getItemInStorageIdUserInput() {
        System.out.println(ITEM_IN_STORAGE_ID_COMMAND);
        int userInput = requestIntUserInput();

        while (!itemsInStorageMap.containsKey(userInput)) {
            System.out.println(INVALID_USER_ID_INPUT);
            userInput = requestIntUserInput();
        }

        return itemsInStorageMap.get(userInput).getId();
    }

    private Result selectItem() {
        int itemInStorageId = getItemInStorageIdUserInput();
        return Result.successful(UiAction.SHOW_SCREEN, "", new ItemDetailsScreen
                (itemsInStorageControllerInstance.getItemInStorage(itemInStorageId), compartmentName, this.storageId));
    }

    private Result addItemInStorage() {
        String itemName = getNameFromUser();
        int quantity = getQuantityFromUser();
        LocalDate addedDate = getAddedDateFromUser();
        LocalDate expiryDate = getExpiryDateFromUser();

        ItemsInStorage item = new ItemsInStorage.Builder().setName(itemName)
                .setQuantity(quantity).setCompartmentId(compartmentId)
                .setDateAdded(addedDate).setDateOfExpiration(expiryDate).build();

        return itemsInStorageControllerInstance.addItemInStorage(item)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully added!",
                this)
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyItemInStorage() {
        int itemId = getItemInStorageIdUserInput();

        System.out.println(CURRENT_VALUE + displayUserRelevantDetails(itemId));
        String itemName = getNameFromUser();
        int quantity = getQuantityFromUser();
        LocalDate addedDate = getAddedDateFromUser();
        LocalDate expiryDate = getExpiryDateFromUser();

        ItemsInStorage item = new ItemsInStorage.Builder().setName(itemName)
                .setQuantity(quantity).setCompartmentId(compartmentId)
                .setDateAdded(addedDate).setDateOfExpiration(expiryDate).setId(itemId).build();

        return itemsInStorageControllerInstance.modifyItemInStorage(item)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully updated!",
                this)
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result deleteItemInStorage() {
        int itemId = getItemInStorageIdUserInput();
        System.out.printf("%s DELETE %s%n%s", USER_ACTION_EXPLANATION, itemsInStorageControllerInstance.getItemInStorage(itemId).getItemInStorageName(), CONFIRMATION_USER_QUERY);
        String confirmationUserInput = requestUserInput();
        return confirmationUserInput.equals(AFFIRMATIVE_CODE) && itemsInStorageControllerInstance.deleteItemInStorage(itemId)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully deleted!",
                this)
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation cancelled, please choose a different action.",
                this);
    }

    private Result deleteAllItemsInStorage() {
        System.out.printf("%s DELETE ALL Items!%n%s", USER_ACTION_EXPLANATION, CONFIRMATION_USER_QUERY);

        String userInput = requestUserInput();
        if (userInput.equals(AFFIRMATIVE_CODE)) {
            List<Integer> itemsIds = new ArrayList<>();
            List<ItemsInStorage> allItems = itemsInStorageControllerInstance.getAllItemsInStorage(compartmentId);
            for (ItemsInStorage item : allItems) {
                itemsIds.add(item.getId());
            }
            getNamesAndIdsOfItems(compartmentId);
            itemsInStorageControllerInstance.deleteItemsInStorage(itemsIds);
            return Result.successful(UiAction.SHOW_SCREEN,
                    "All items successfully deleted!",
                    this);
        } else if (userInput.equals(NEGATIVE_CODE)) {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation cancelled, please choose a different action.",
                    this);
        } else {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        }
    }
}


