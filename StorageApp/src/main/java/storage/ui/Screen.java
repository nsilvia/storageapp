package storage.ui;

import storage.ui.controller.Result.Result;

public interface Screen {
    Result displayUiAndHandleUserInput();

    Result displayGreetingUiAndHandleUserInput();

    String requestUserInput();

    int requestIntUserInput();

    void showMessage(String message);

    String displayUserRelevantDetails(int id);

    void displayGreetingMessage();

    void displayScreenDetails();

    void displayLegendInstructions();

    String expectUserCommand();

    Result runUserCommand(String userCommand);
}
