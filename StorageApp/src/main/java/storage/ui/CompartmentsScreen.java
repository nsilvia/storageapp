package storage.ui;

import storage.controller.CompartmentsControllerImpl;
import storage.controller.StorageControllerImpl;
import storage.data.Compartment;
import storage.ui.controller.Result.Result;
import storage.ui.controller.Result.UiAction;

import java.util.*;

public class CompartmentsScreen extends AbstractScreen {
    private static final String COMMAND_MESSAGE = "Type here your command: ";
    private static final String COMPARTMENT_NAME_COMMAND = "Set Compartment NAME: ";
    private static final String COMPARTMENT_ID_COMMAND = "Select Compartment ID: ";
    private static final String COMPARTMENT_POSITION_COMMAND = "Set Compartment POSITION: ";

    private final Map<Integer, Compartment> compartmentsMap = new HashMap<>();
    private final StorageControllerImpl storageControllerInstance = StorageControllerImpl.getInstance();
    private final CompartmentsControllerImpl compartmentsControllerInstance = CompartmentsControllerImpl.getInstance();
    private final int storageId;

    CompartmentsScreen(int storageId) {
        this.storageId = storageId;
    }

    private String getStorageName() {
        return storageControllerInstance.getStorage(storageId).getStorageName();
    }

    @Override
    public String displayUserRelevantDetails(int id) {
        Compartment currentCompartment = compartmentsControllerInstance.getCompartment(id);
        return String.format("compartmentName = %s, position = %s%n", currentCompartment.getCompartmentName(), currentCompartment.getPosition());
    }

    @Override
    public void displayGreetingMessage() {
        System.out.printf("%s %s %s%n", SCREEN_SEPARATOR, getStorageName(), SCREEN_SEPARATOR);
    }

    @Override
    public void displayScreenDetails() {
        System.out.printf("%s%n%s", displayNumberOfCompartments(), getNamesAndIdsOfCompartments());
    }

    @Override
    public void displayLegendInstructions() {
        String format = "%1$-29s%2$-1s%n";
        System.out.println(LEGEND_SEPARATOR);
        System.out.printf(format, "ACTION", "COMMAND");
        System.out.printf(format, SELECT_MESSAGE, SELECT_CODE);
        System.out.printf(format, ADD_MESSAGE, ADD_CODE);
        System.out.printf(format, MODIFY_MESSAGE, MODIFY_CODE);
        System.out.printf(format, DELETE_MESSAGE, DELETE_CODE);
        System.out.printf(format, DELETE_ALL_MESSAGE, DELETE_ALL_CODE);
        System.out.printf(format, BACK_COMMAND, BACK_CODE);
        System.out.printf(format, CLOSE_APP_MESSAGE, CLOSE_APP_CODE);
        System.out.println(LEGEND_SEPARATOR);
    }

    @Override
    public String expectUserCommand() {
        System.out.println(COMMAND_MESSAGE);
        return requestUserInput();
    }

    @Override
    public Result runUserCommand(String userInputCodeCommand) {
        switch (userInputCodeCommand) {
            case BACK_CODE:
                return Result.successful(UiAction.SHOW_SCREEN, "", new StorageScreen());
            case ADD_CODE:
                return addCompartment();
            case MODIFY_CODE:
                return modifyCompartment();
            case DELETE_CODE:
                return deleteCompartment();
            case DELETE_ALL_CODE:
                return deleteAllCompartments();
            case SELECT_CODE:
                return selectCompartment();
            case CLOSE_APP_CODE:
                return Result.successful(UiAction.EXIT, "Goodbye!", this);
            default:
                return Result.failed(UiAction.SHOW_SCREEN, INVALID_GENERAL_INPUT, this);
        }
    }

    private String displayNumberOfCompartments() {
        return String.format("Number of Compartments: %d%n", compartmentsControllerInstance.getAllCompartmentsInStorage(storageId).size());
    }

    private String getNamesAndIdsOfCompartments() {
        StringBuilder messageWithAllCompartments = new StringBuilder();
        int generatedCompartmentId = 0;

        for (Compartment comp : compartmentsControllerInstance.getAllCompartmentsInStorage(storageId)) {
            generatedCompartmentId++;
            compartmentsMap.put(generatedCompartmentId, comp);
            messageWithAllCompartments.append(
                    String.format("Compartment Id: %d; Name: %s; Position: %s%n", generatedCompartmentId, comp.getCompartmentName(), comp.getPosition()));
        }
        return messageWithAllCompartments.toString();
    }

    private int getCompartmentIdUserInput() throws UserInputException {
        System.out.println(COMPARTMENT_ID_COMMAND);
        int userInput = requestIntUserInput();

        if (!compartmentsMap.containsKey(userInput)) {
            throw new UserInputException(INVALID_USER_ID_INPUT);
        }
        return compartmentsMap.get(userInput).getId();
    }

    private String getCompartmentNameFromUser() {
        System.out.println(COMPARTMENT_NAME_COMMAND);
        return requestUserInput();
    }

    private String getCompartmentPosition() {
        System.out.println(COMPARTMENT_POSITION_COMMAND);
        return requestUserInput();
    }

    private Result selectCompartment() {
        try {
            int compartmentId = getCompartmentIdUserInput();
            AbstractScreen screen = new ItemsInStorageScreen(storageId, compartmentId,
                    compartmentsControllerInstance.getCompartment(compartmentId).getCompartmentName());
            return Result.successful(UiAction.SHOW_SCREEN, "", screen);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result addCompartment() {
        String compartmentName = getCompartmentNameFromUser();
        String compartmentPosition = getCompartmentPosition();

        Compartment compartment = new Compartment.Builder().setCompartmentName(compartmentName)
                .setStorageId(storageId)
                .setPosition(compartmentPosition).build();

        return compartmentsControllerInstance.addCompartment(compartment)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Compartment successfully added!",
                this)
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyCompartment() {
        try {
            int compartmentId = getCompartmentIdUserInput();

            System.out.printf("%s %s", CURRENT_VALUE, displayUserRelevantDetails(compartmentId));
            String compartmentName = getCompartmentNameFromUser();
            String compartmentPosition = getCompartmentPosition();

            Compartment compartment = new Compartment.Builder().setCompartmentName(compartmentName)
                    .setStorageId(storageId).setId(compartmentId).setPosition(compartmentPosition).build();

            return compartmentsControllerInstance.modifyCompartment(compartment)
                    ? Result.successful(UiAction.SHOW_SCREEN,
                    "Compartment successfully updated!",
                    this)
                    : Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result deleteCompartment() {
        try {
            int compartmentId = getCompartmentIdUserInput();

            System.out.printf("%s delete %s%n%s", USER_ACTION_EXPLANATION, compartmentsControllerInstance.getCompartment(compartmentId).getCompartmentName(),
                    CONFIRMATION_USER_QUERY);
            String confirmationUserInput = requestUserInput();

            return confirmationUserInput.equals(AFFIRMATIVE_CODE) && compartmentsControllerInstance.deleteCompartment(compartmentId)
                    ? Result.successful(UiAction.SHOW_SCREEN,
                    "Storage successfully deleted!",
                    this)
                    : Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result deleteAllCompartments() {
        System.out.printf("WARNING! YOU ARE ABOUT TO DELETE ALL COMPARTMENTS!%nALL ITEMS WILL BE LOST!%n%s",
                CONFIRMATION_USER_QUERY);

        String userInput = requestUserInput();
        if (userInput.equals("y")) {
            List<Integer> compartmentIds = new ArrayList<>();
            List<Compartment> allCompartments =
                    compartmentsControllerInstance.getAllCompartmentsInStorage(storageId);
            for (Compartment compartment : allCompartments) {
                compartmentIds.add(compartment.getId());
            }
            getNamesAndIdsOfCompartments();
            compartmentsControllerInstance.deleteCompartments(compartmentIds);
            return Result.successful(UiAction.SHOW_SCREEN,
                    "All compartments successfully deleted!",
                    this);
        } else if (userInput.equals(NEGATIVE_CODE)) {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation cancelled, please choose a different action.",
                    this);
        } else {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        }

    }
}

