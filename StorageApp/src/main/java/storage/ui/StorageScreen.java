package storage.ui;

import storage.controller.StorageControllerImpl;
import storage.data.Storage;
import storage.ui.controller.Result.Result;
import storage.ui.controller.Result.UiAction;

import java.util.*;

public class StorageScreen extends AbstractScreen {
    private static final String STORAGE_ID_COMMAND = "Select Storage ID: ";
    private static final String STORAGE_NAME_COMMAND = "Set new storage name: ";
    private static final String STORAGE_COMPARTMENT_NUMBER_COMMAND = "Set new numbers of compartments: ";

    private Storage currentStorage;

    private final StorageControllerImpl storageControllerInstance = StorageControllerImpl.getInstance();

    private final Map<Integer, Storage> storagesMap = new HashMap<>();

    @Override
    public String displayUserRelevantDetails(int id) {
        currentStorage = storageControllerInstance.getStorage(id);
        return String.format("name = %s%n", currentStorage.getStorageName());
    }

    @Override
    public void displayGreetingMessage() {
        System.out.printf("%s Storages %s%n", SCREEN_SEPARATOR, SCREEN_SEPARATOR);
    }

    @Override
    public void displayScreenDetails() {
        System.out.printf("%s%n%s%n", displayNumberOfStorages(), getNamesAndIdsOfStorages());
    }

    @Override
    public void displayLegendInstructions() {
        String format = "%1$-29s%2$-1s%n";
        System.out.println(LEGEND_SEPARATOR);
        System.out.printf(format, "ACTION", "COMMAND");
        System.out.printf(format, SELECT_MESSAGE, SELECT_CODE);
        System.out.printf(format, ADD_MESSAGE, ADD_CODE);
        System.out.printf(format, MODIFY_MESSAGE, MODIFY_CODE);
        System.out.printf(format, DELETE_MESSAGE, DELETE_CODE);
        System.out.printf(format, DELETE_ALL_MESSAGE, DELETE_ALL_CODE);
        System.out.printf(format, CLOSE_APP_MESSAGE, CLOSE_APP_CODE);
        System.out.println(LEGEND_SEPARATOR);
    }

    @Override
    public String expectUserCommand() {
        System.out.println(COMMAND_MESSAGE);
        return requestUserInput();
    }

    @Override
    public Result runUserCommand(String userInputCodeCommand) {
        switch (userInputCodeCommand) {
            case BACK_CODE:
                return Result.failed(UiAction.SHOW_SCREEN,
                        "Back option not available for this screen!",
                        this);
            case ADD_CODE:
                return addStorage();
            case MODIFY_CODE:
                return modifyStorage();
            case DELETE_CODE:
                return deleteStorage();
            case DELETE_ALL_CODE:
                return deleteAllStorages();
            case SELECT_CODE:
                return selectStorage();
            case CLOSE_APP_CODE:
                return Result.successful(UiAction.EXIT,
                        "Goodbye!",
                        this);
            default:
                return Result.failed(UiAction.SHOW_SCREEN, INVALID_GENERAL_INPUT, this);
        }
    }

    private String displayNumberOfStorages() {
        return String.format("Number of Storages: %d%n", storageControllerInstance.getAllStorages().size());

    }

    private String getNamesAndIdsOfStorages() {
        List<Storage> storages = storageControllerInstance.getAllStorages();
        StringBuilder messageWithAllStorages = new StringBuilder();
        int generatedStorageId = 0;

        for (Storage storage : storages) {
            generatedStorageId++;
            storagesMap.put(generatedStorageId, storage);
            messageWithAllStorages.append(
                    String.format("Storage id: %d, Storage name: %s;%n", generatedStorageId, storage.getStorageName())
            );
        }
        return messageWithAllStorages.toString();
    }

    private String getStorageNameFromUser() {
        System.out.println(STORAGE_NAME_COMMAND);
        return requestUserInput();
    }

    private int getStorageCompartmentsNumberFromUser() {
        System.out.println(STORAGE_COMPARTMENT_NUMBER_COMMAND);
        return requestIntUserInput();
    }

    private int getStorageIdUserInput() throws UserInputException {
        System.out.println(STORAGE_ID_COMMAND);
        int userInput = requestIntUserInput();
        if(!storagesMap.containsKey(userInput)){
            throw new UserInputException(INVALID_USER_ID_INPUT);
        }
        return storagesMap.get(userInput).getId();
    }

    private Result selectStorage() {
        try {
            int storageId = getStorageIdUserInput();
            AbstractScreen newScreen = new CompartmentsScreen(storageId);
            return Result.successful(UiAction.SHOW_SCREEN, "", newScreen);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result addStorage() {
        String storageName = getStorageNameFromUser();
        int compartmentNumber = getStorageCompartmentsNumberFromUser();
        Storage storage = new Storage.Builder().setStorageName(storageName).build();

        return storageControllerInstance.addStorage(storage)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Storage successfully added!",
                this)
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyStorage() {
        try {
            int storageId = getStorageIdUserInput();

            if (storageId == -1) {
                return Result.failed(UiAction.SHOW_SCREEN, "", this);
            }

            System.out.printf("%s modify %s;%n", USER_ACTION_EXPLANATION, storageControllerInstance.getStorage(storageId).getStorageName());
            System.out.printf("%s %s", CURRENT_VALUE, displayUserRelevantDetails(storageId));

            String storageName = getStorageNameFromUser();
            int compartmentNumber = getStorageCompartmentsNumberFromUser();

            Storage storage = new Storage.Builder().setStorageName(storageName)
                    .setId(storageId).build();

            return storageControllerInstance.modifyStorage(storage)
                    ? Result.successful(UiAction.SHOW_SCREEN,
                    "Storage successfully updated!",
                    this)
                    : Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result deleteStorage() {
        try {
            int storageId = getStorageIdUserInput();

            if (storageId == -1) {
                return Result.failed(UiAction.SHOW_SCREEN, "", this);
            }

            System.out.printf("%s delete %s%n%s", USER_ACTION_EXPLANATION, storageControllerInstance.getStorage(storageId).getStorageName(),
                    CONFIRMATION_USER_QUERY);
            String confirmationUserInput = requestUserInput();

            return confirmationUserInput.equals(AFFIRMATIVE_CODE) && storageControllerInstance.deleteStorage(storageId)
                    ? Result.successful(UiAction.SHOW_SCREEN,
                    "Storage successfully deleted!",
                    this)
                    : Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        } catch (UserInputException e) {
            return Result.failed(UiAction.SHOW_SCREEN, "", this);
        }
    }

    private Result deleteAllStorages() {
        System.out.printf("WARNING! YOU ARE ABOUT TO DELETE ALL STORAGES!%nALL COMPARTMENTS AND ITEMS WILL BE LOST!%n%s", CONFIRMATION_USER_QUERY);

        String userInput = requestUserInput();
        if (userInput.equals("y")) {
            List<Integer> storageIds = new ArrayList<>();
            List<Storage> allStorages = storageControllerInstance.getAllStorages();
            for (Storage storage : allStorages) {
                storageIds.add(storage.getId());
            }
            getNamesAndIdsOfStorages();
            int numberOfDeletedStorages = storageControllerInstance.deleteStorages(storageIds);
            return Result.successful(UiAction.SHOW_SCREEN,
                    String.format("You have deleted %s storage(s).", numberOfDeletedStorages),
                    this);
        } else if (userInput.equals(NEGATIVE_CODE)) {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation cancelled, please choose a different action.",
                    this);
        } else {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        }
    }
}


