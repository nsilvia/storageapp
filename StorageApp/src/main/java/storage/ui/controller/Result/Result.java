package storage.ui.controller.Result;

import storage.ui.AbstractScreen;

public class Result {
    private final boolean isSuccessful;
    private final String message;
    private final UiAction uiAction;
    private final AbstractScreen screen;

    public static Result successful(UiAction uiAction, String message, AbstractScreen screen){
        return new Result(true, uiAction, message, screen);
    }
    
    public static Result failed(UiAction uiAction, String message, AbstractScreen screen){
        return new Result(false, uiAction, message, screen);
    }

    private Result(boolean isSuccessful, UiAction uiAction, String message, AbstractScreen screen){
        this.isSuccessful = isSuccessful;
        this.uiAction = uiAction;
        this.message = message;
        this.screen = screen;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public String getMessage() {
        return message;
    }

    public UiAction getUiAction() {
        return uiAction;
    }

    public AbstractScreen getScreen() {
        return screen;
    }
}
