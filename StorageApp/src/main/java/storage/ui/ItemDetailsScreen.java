package storage.ui;

import storage.controller.ItemsControllerImpl;
import storage.data.ItemsInStorage;
import storage.ui.controller.Result.Result;
import storage.ui.controller.Result.UiAction;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ItemDetailsScreen extends ItemsInStorageScreen {
    private static final String DATE_ADDED_CODE = "dadd";
    private static final String EXPIRY_DATE_CODE = "dexp";
    private static final String QUANTITY_CODE = "qty";
    private static final String NAME_CODE = "name";
    private static final String MODIFY_NAME_MESSAGE = "To MODIFY the NAME type: ";
    private static final String MODIFY_ADDED_DATE_MESSAGE = "To MODIFY the ADDED DATE type: ";
    private static final String DELETE_SELECTED_ITEM_MESSAGE = "To DELETE the selected item type: ";
    private static final String MODIFY_EXPIRY_DATE_MESSAGE = "To MODIFY the EXPIRY DATE type: ";
    private static final String MODIFY_QUANTITY_MESSAGE = "To MODIFY the QUANTITY type: ";
    private static final String COMMAND_MESSAGE = "Type here your command: ";

    private final ItemsControllerImpl itemsController = ItemsControllerImpl.getInstance();
    private final ItemsInStorage currentItem;
    private final String compartmentName;
    private final int storageId;

    public ItemDetailsScreen(ItemsInStorage item, String compartmentName, int storageId) {
        super(storageId, item.getCompartmentId(), compartmentName);
        this.currentItem = item;
        this.compartmentName = compartmentName;
        this.storageId = storageId;
    }

    @Override
    public void displayGreetingMessage() {
        System.out.printf("%s %s %s%n", SCREEN_SEPARATOR, currentItem.getItemInStorageName(), SCREEN_SEPARATOR);
    }

    @Override
    public void displayScreenDetails() {
        System.out.println(displayItemDetails());
    }

    @Override
    public void displayLegendInstructions() {
        String format = "%1$-37s%2$-1s%n";
        System.out.println(LEGEND_SEPARATOR_TITLE);
        System.out.printf(format, "ACTION", "COMMAND");
        System.out.printf(format, MODIFY_NAME_MESSAGE, NAME_CODE);
        System.out.printf(format, MODIFY_ADDED_DATE_MESSAGE, DATE_ADDED_CODE);
        System.out.printf(format, MODIFY_EXPIRY_DATE_MESSAGE, EXPIRY_DATE_CODE);
        System.out.printf(format, MODIFY_QUANTITY_MESSAGE, QUANTITY_CODE);
        System.out.printf(format, DELETE_SELECTED_ITEM_MESSAGE, DELETE_CODE);
        System.out.printf(format, BACK_COMMAND, BACK_CODE);
        System.out.printf(format, CLOSE_APP_MESSAGE, CLOSE_APP_CODE);
        System.out.println(LEGEND_SEPARATOR);
    }

    @Override
    public String expectUserCommand() {
        System.out.println(COMMAND_MESSAGE);
        return requestUserInput();
    }

    @Override
    public Result runUserCommand(String userInputCodeCommand) {
        switch (userInputCodeCommand) {
            case BACK_CODE:
                return Result.successful(UiAction.SHOW_SCREEN,
                        "",
                        new ItemsInStorageScreen(storageId, currentItem.getCompartmentId(), compartmentName));
            case DATE_ADDED_CODE:
                return modifyDateAdded();
            case EXPIRY_DATE_CODE:
                return modifyExpiryDate();
            case QUANTITY_CODE:
                return modifyQuantity();
            case DELETE_CODE:
                return deleteCurrentItem();
            case NAME_CODE:
                return modifyName();
            case CLOSE_APP_CODE:
                return Result.successful(UiAction.EXIT, "Goodbye!", this);
            default:
                return Result.failed(UiAction.SHOW_SCREEN, INVALID_GENERAL_INPUT, this);
        }
    }

    private String displayItemDetails() {
        return String.format("name = %s, dateAdded = %s, dateOfExpiration = %s, quantity = %s%n",
                currentItem.getItemInStorageName(), DateTimeFormatter.ISO_DATE.format(currentItem.getDateAdded()),
                DateTimeFormatter.ISO_DATE.format(currentItem.getDateOfExpiration()), currentItem.getQuantity());
    }

    private Result modifyDateAdded() {
        LocalDate dateAddedUserInput = getAddedDateFromUser();

        ItemsInStorage modifiedItem = new ItemsInStorage.Builder().setName(currentItem.getItemInStorageName())
                .setQuantity(currentItem.getQuantity()).setCompartmentId(currentItem.getCompartmentId())
                .setDateAdded(dateAddedUserInput).setDateOfExpiration(currentItem.getDateOfExpiration()).setId(currentItem.getId()).build();

        return itemsController.modifyItemInStorage(modifiedItem)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully updated!",
                new ItemDetailsScreen(modifiedItem, compartmentName, storageId))
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyExpiryDate() {
        LocalDate expiryDateUserInput = getAddedDateFromUser();

        ItemsInStorage modifiedItem = new ItemsInStorage.Builder().setName(currentItem.getItemInStorageName())
                .setQuantity(currentItem.getQuantity()).setCompartmentId(currentItem.getCompartmentId())
                .setDateAdded(currentItem.getDateAdded()).setDateOfExpiration(expiryDateUserInput).setId(currentItem.getId()).build();

        return itemsController.modifyItemInStorage(modifiedItem)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully updated!",
                new ItemDetailsScreen(modifiedItem, compartmentName, storageId))
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyQuantity() {
        int quantityUserInput = getQuantityFromUser();

        ItemsInStorage modifiedItem = new ItemsInStorage.Builder().setName(currentItem.getItemInStorageName())
                .setQuantity(quantityUserInput).setCompartmentId(currentItem.getCompartmentId())
                .setDateAdded(currentItem.getDateAdded()).setDateOfExpiration(currentItem.getDateOfExpiration()).setId(currentItem.getId()).build();

        return itemsController.modifyItemInStorage(modifiedItem)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully updated!",
                new ItemDetailsScreen(modifiedItem, compartmentName, storageId))
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result modifyName() {
        String nameUserInput = getNameFromUser();

        ItemsInStorage modifiedItem = new ItemsInStorage.Builder().setName(nameUserInput)
                .setQuantity(currentItem.getQuantity()).setCompartmentId(currentItem.getCompartmentId())
                .setDateAdded(currentItem.getDateAdded()).setDateOfExpiration(currentItem.getDateOfExpiration()).setId(currentItem.getId()).build();

        return itemsController.modifyItemInStorage(modifiedItem)
                ? Result.successful(UiAction.SHOW_SCREEN,
                "Item successfully updated!",
                new ItemDetailsScreen(modifiedItem, compartmentName, storageId))
                : Result.failed(UiAction.SHOW_SCREEN,
                "Operation failed, please try again later.",
                this);
    }

    private Result deleteCurrentItem() {
        System.out.println(USER_ACTION_EXPLANATION + " delete " + currentItem.getItemInStorageName());
        System.out.println(CONFIRMATION_USER_QUERY);
        String confirmationUserInput = requestUserInput();

        if (confirmationUserInput.equals(AFFIRMATIVE_CODE)) {
            if (itemsController.deleteItemInStorage(currentItem.getId())) {
                return Result.successful(UiAction.SHOW_SCREEN,
                        "Item successfully deleted!",
                        this);
            } else {
                return Result.failed(UiAction.SHOW_SCREEN,
                        "Operation failed, please try again later.",
                        this);
            }
        } else {
            return Result.failed(UiAction.SHOW_SCREEN,
                    "Operation failed, please try again later.",
                    this);
        }
    }
}