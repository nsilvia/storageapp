package storage.ui;

import storage.ui.controller.Result.Result;

import java.util.ArrayList;
import java.util.List;

public class UiMain {
    private static final String START_APP_MESSAGE = "|||----------Welcome to your Storage App----------||| \n";

    public static void main(String[] args) {
        System.out.println(START_APP_MESSAGE);
        startUi(getFirstScreen());
        //todo remove password from DBConnection class
        //todo when command is typed, like select, but there is nothing to select, you cannot change command
        //todo allow multiple words added when user inputs

        //todo reduce visibility of variables
        //todo remove unused variables
    }

    private static void startUi(AbstractScreen screen) {
        Result result = screen.displayGreetingUiAndHandleUserInput();
        while(!result.isSuccessful()){
            screen.showMessage(result.getMessage());
            result = screen.displayUiAndHandleUserInput();
        }

        switch (result.getUiAction()){
            case EXIT:
                System.exit(0);
                return;
            case SHOW_SCREEN:
                startUi(result.getScreen());
                break;
        }
    }

    private static AbstractScreen getFirstScreen() {
        return new StorageScreen();
    }
}