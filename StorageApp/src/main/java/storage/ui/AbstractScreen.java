package storage.ui;

import storage.ui.controller.Result.Result;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public abstract class AbstractScreen implements Screen {
    private static final String INVALID_INT_VALUE = "Invalid - Integers only - Try again: ";
    static final String COMMAND_MESSAGE = "Type command: ";
    static final String INVALID_USER_ID_INPUT = "Invalid ID - Try again: ";
    static final String INVALID_GENERAL_INPUT = "Invalid Command - Try again: ";
    static final String CURRENT_VALUE = "Current value: ";
    static final String BACK_COMMAND = "To go BACK type: ";
    static final String BACK_CODE = "back";
    static final String SELECT_CODE = "sel";
    static final String ADD_CODE = "add";
    static final String MODIFY_CODE = "mod";
    static final String DELETE_CODE = "del";
    static final String DELETE_ALL_CODE = "delall";
    static final String AFFIRMATIVE_CODE = "y";
    static final String NEGATIVE_CODE = "n";
    static final String CLOSE_APP_CODE = "exit";

    String SELECT_MESSAGE = "To SELECT type: ";
    String ADD_MESSAGE = "To ADD type: ";
    String MODIFY_MESSAGE = "To MODIFY type: ";
    String DELETE_MESSAGE = "To DELETE one entry type: ";
    String DELETE_ALL_MESSAGE = "To DELETE ALL type: ";
    String USER_ACTION_EXPLANATION = "You are about to ";
    String CONFIRMATION_USER_QUERY = String.format("To continue type: y %nTo cancel type: n%n");
    String SCREEN_SEPARATOR = "==================";
    String CLOSE_APP_MESSAGE = "To CLOSE app type: ";
    String LEGEND_SEPARATOR = String.format("--------------------------------------------%n");
    String LEGEND_SEPARATOR_TITLE = String.format("------------------ LEGEND ------------------%n");

    @Override
    public final Result displayUiAndHandleUserInput() {
        displayScreenDetails();
        displayLegendInstructions();
        return runUserCommand(expectUserCommand());
    }
    @Override
    public final Result displayGreetingUiAndHandleUserInput(){
        displayGreetingMessage();
        return displayUiAndHandleUserInput();
    }

    @Override
    public String requestUserInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public int requestIntUserInput() {
        Scanner scanner;
        int userInput = -1;
        while (userInput == -1) {
            try {
                scanner = new Scanner(System.in);
                userInput = scanner.nextInt();
            } catch (InputMismatchException ime) {
                System.out.println(INVALID_INT_VALUE);
            } catch (NoSuchElementException | IllegalStateException e) {
                System.out.printf("An unknown error has occurred. %nPlease try again.");
            }
        }
        return userInput;
    }

    @Override
    public final void showMessage(String message) {
        System.out.println(message);
        System.out.println(LEGEND_SEPARATOR);
    }

    @Override
    public abstract String displayUserRelevantDetails(int id);

    @Override
    public abstract void displayGreetingMessage();

    @Override
    public abstract void displayScreenDetails();

    @Override
    public abstract void displayLegendInstructions();

    @Override
    public abstract String expectUserCommand();

    @Override
    public abstract Result runUserCommand(String userCommand);

}