package storage.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ItemsInStorage {
    private final int id;
    private String name;
    private final int compartmentId;
    private LocalDate dateAdded;
    private LocalDate dateOfExpiration;
    private final int quantity;

    private ItemsInStorage(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.compartmentId = builder.compartmentId;
        this.dateAdded = builder.dateAdded;
        this.dateOfExpiration = builder.dateOfExpiration;
        this.quantity = builder.quantity;
    }

    public int getId() {
        return id;
    }

    public String getItemInStorageName() {
        if (name == null) {
            return name = "default";
        }
        return name;
    }

    public int getCompartmentId() {
        return compartmentId;
    }

    public LocalDate getDateAdded() {
        if (dateAdded == null) {
            return dateAdded = LocalDate.of(1900, 1, 1);
        }
        return dateAdded;
    }

    public LocalDate getDateOfExpiration() {
        if (dateAdded == null) {
            return dateOfExpiration = LocalDate.of(1900, 1, 1);
        }
        return dateOfExpiration;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ItemsInStorage)) {
            return false;
        }
        ItemsInStorage other = (ItemsInStorage) obj;
        return this.compartmentId == other.compartmentId &&
                this.dateAdded.equals(other.dateAdded) &&
                this.dateOfExpiration.equals(other.dateOfExpiration) &&
                this.quantity == other.quantity;

    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        hashCode += hashCode * 7 + compartmentId;
        hashCode += hashCode * 7 + dateAdded.hashCode();
        hashCode += hashCode * 7 + dateOfExpiration.hashCode();
        hashCode += hashCode * 7 + quantity;
        return hashCode;
    }

    @Override
    public String toString() {
        return "name = " + name +
                ", id = " + id +
                ", compartmentId = " + compartmentId +
                ", dateAdded = " + DateTimeFormatter.ISO_DATE.format(dateAdded) +
                ", dateOfExpiration = " + DateTimeFormatter.ISO_DATE.format(dateOfExpiration) +
                ", quantity = " + quantity;
    }

    public static class Builder {
        private int id;
        private String name;
        private int compartmentId;
        private LocalDate dateAdded;
        private LocalDate dateOfExpiration;
        private int quantity;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            if (name == null) {
                this.name = "default";
            } else {
                this.name = name;
            }
            return this;
        }

        public Builder setCompartmentId(int compartmentId) {
            this.compartmentId = compartmentId;
            return this;
        }

        public Builder setDateAdded(LocalDate dateAdded) {
            if (dateAdded == null) {
                this.dateAdded = LocalDate.of(1900, 1, 1);
            } else {
                this.dateAdded = dateAdded;
            }
            return this;
        }

        public Builder setDateOfExpiration(LocalDate dateOfExpiration) {
            if (dateOfExpiration == null) {
                this.dateOfExpiration = LocalDate.of(1990, 1, 1);
            } else {
                this.dateOfExpiration = dateOfExpiration;
            }
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public ItemsInStorage build() {
            return new ItemsInStorage(this);
        }
    }

}
