package storage.data;

public class Storage {
    private final int id;
    private final String storageName;

    private Storage(Builder builder){
        id = builder.id;
        storageName = builder.storageName;
    }

    public int getId() {
        return id;
    }

    public String getStorageName() {
        return storageName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Storage)) {
            return false;
        }
        Storage other = (Storage) obj;
        return this.storageName.equals(other.storageName);
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        hashCode += hashCode * 7 + storageName.hashCode();
        return hashCode;
    }

    @Override
    public String toString() {
        return "storageName = " + storageName;
    }

    public static class Builder {
        private int id;
        private String storageName;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setStorageName(String storageName) {
            this.storageName = storageName;
            return this;
        }

        public Storage build(){
            return new Storage(this);
        }
    }
}