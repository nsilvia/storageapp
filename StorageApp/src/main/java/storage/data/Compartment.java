package storage.data;

public class Compartment {
    private final int id;
    private final String compartmentName;
    private final String position;
    private final int storageId;

    private Compartment(Builder builder){
        this.id = builder.id;
        this.compartmentName = builder.compartmentName;
        this.position = builder.position;
        this.storageId = builder.storageId;
    }

    public int getId() {
        return id;
    }

    public String getCompartmentName() {
        return compartmentName;
    }

    public String getPosition() {
        return position;
    }

    public int getStorageId() {
        return storageId;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Compartment)){
            return false;
        }
        Compartment other = (Compartment) obj;
        return this.position.equals(other.position) &&
                this.compartmentName.equals(other.compartmentName) &&
                this.storageId == other.storageId;
    }

    @Override
    public int hashCode() {
        int hashCode = 1;

        hashCode += hashCode*7 + storageId;
        hashCode += hashCode*7 + compartmentName.hashCode();
        hashCode += hashCode*7 + position.hashCode();

        return hashCode;
    }

    @Override
    public String toString() {
        return "id = " + id + ", compartmentName = " + compartmentName +
                ", position = " + position + ", storageId = " +storageId;

    }

    public static class Builder{
        private int id;
        private String compartmentName;
        private String position;
        private int storageId;

        public Builder setId(int id){
            this.id = id;
            return this;
        }

        public Builder setCompartmentName(String compartmentName){
            this.compartmentName = compartmentName;
            return this;
        }

        public Builder setPosition(String position){
            this.position = position;
            return this;
        }

        public Builder setStorageId(int storageId){
            this.storageId = storageId;
            return this;
        }

        public Compartment build(){
            return new Compartment(this);
        }


    }

}
