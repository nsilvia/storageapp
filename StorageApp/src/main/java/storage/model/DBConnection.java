package storage.model;

import java.sql.*;
import java.util.Properties;

public class DBConnection {
    static Connection getConnection() throws SQLException {
        try {
            Class<?> aClass = Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Properties connectionProps = new Properties();
        connectionProps.put("user", "silvia");
        connectionProps.put("password", "hisilvia123");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/Storage?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                connectionProps);
    }
}
