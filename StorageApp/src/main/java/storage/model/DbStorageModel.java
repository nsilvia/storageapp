package storage.model;

import storage.data.Storage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static storage.model.DBConnection.getConnection;

public class DbStorageModel implements StorageModel {

    private static final DbStorageModel storageModelInstance = new DbStorageModel();
    private static final String SELECT_ALL_QUERY = "SELECT * FROM Storage";
    private static final String STORAGE_NAME_COLUMN = "storageName";
    private static final String STORAGE_ID_COLUMN = "id";
    private static final String SELECT_STORAGE_QUERY = "SELECT * FROM Storage WHERE "
            + STORAGE_ID_COLUMN + " = ?";

    private static final String INSERT_QUERY = "INSERT INTO Storage (" + STORAGE_NAME_COLUMN + ") VALUES (?)";
    private static final String DELETE_QUERY = "DELETE FROM Storage WHERE " + STORAGE_ID_COLUMN + " = ?";
    private static final String MODIFY_QUERY = "UPDATE Storage SET " +
            STORAGE_NAME_COLUMN + " = ? WHERE " + STORAGE_ID_COLUMN + " = ? ";

    public static DbStorageModel getStorageModelInstance() {
        return storageModelInstance;
    }

    private DbStorageModel() {}

    public String setNumberOfElements(List<Integer> ids) {
        StringBuilder elements = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i != ids.size() - 1) {
                elements.append("?, ");
            } else {
                elements.append("?");
            }
        }
        return elements.toString();
    }

    @Override
    public List<Storage> getAllStorages() {
        List<Storage> storages = new ArrayList<>();
        try (
                Connection conn = getConnection();
                Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_QUERY)
        ) {
            while (resultSet.next()) {
                Storage.Builder builder = new Storage.Builder();

                Storage storage = builder.setId(resultSet.getInt(STORAGE_ID_COLUMN))
                        .setStorageName(resultSet.getString(STORAGE_NAME_COLUMN))
                        .build();
                storages.add(storage);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return storages;
    }

    @Override
    public Storage getStorage(int storageID) {
        Storage.Builder builder = new Storage.Builder();
        Storage storage = null;
        try (
                Connection conn = getConnection();
                PreparedStatement selectStatement = conn.prepareStatement(SELECT_STORAGE_QUERY)
        ) {
            selectStatement.setInt(1, storageID);
            ResultSet resultSet = selectStatement.executeQuery();
            while (resultSet.next()) {
               storage = builder.setId(resultSet.getInt(STORAGE_ID_COLUMN))
                        .setStorageName(resultSet.getString(STORAGE_NAME_COLUMN))
                        .build();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return storage;
    }


    @Override
    public boolean addStorage(Storage storage) {
        try (
                Connection conn = getConnection();
                PreparedStatement insertStatement = conn.prepareStatement(INSERT_QUERY)
        ) {
            insertStatement.setString(1, storage.getStorageName());

            return insertStatement.executeUpdate() != 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean modifyStorage(Storage storage) {
        try (
                Connection conn = getConnection();
                PreparedStatement updateStorage = conn.prepareStatement(MODIFY_QUERY)
        ) {
            updateStorage.setString(1, storage.getStorageName());
            updateStorage.setInt(2, storage.getId());
            return updateStorage.executeUpdate() != 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteStorage(int id) {
        try (
                Connection conn = getConnection();
                PreparedStatement deleteStatement = conn.prepareStatement(DELETE_QUERY)
        ) {
            deleteStatement.setInt(1, id);
            return deleteStatement.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int deleteStorages(List<Integer> ids) {

        int numberOfCellsAffected = 0;
        String deleteQueryString = "DELETE from Storage WHERE " +
                STORAGE_ID_COLUMN + "IN(" + setNumberOfElements(ids) + ")";
        try (
                Connection conn = getConnection();
                PreparedStatement deleteQuery = conn.prepareStatement(deleteQueryString)
        ) {
            for (int i = 0; i < ids.size(); i++) {
                deleteQuery.setInt(i + 1, ids.get(i));
            }
            numberOfCellsAffected = deleteQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfCellsAffected;
    }
}
