package storage.model;

import storage.data.ItemsInStorage;

import java.util.List;

public interface ItemsInStorageModel {
    List<ItemsInStorage> getAllItemsInStorage(int compartmentId);
    ItemsInStorage getItemInStorage(int itemInStorageId);
    boolean addItemInStorage(ItemsInStorage itemsInStorage);
    boolean modifyItemInStorage(ItemsInStorage itemsInStorage);
    boolean deleteItemInStorage(int id);
    int deleteItemsInStorage(List<Integer> ids);
}
