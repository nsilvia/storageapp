package storage.model;

import storage.data.Compartment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbCompartmentModel implements CompartmentModel {

    private static final DbCompartmentModel compartmentModelInstance = new DbCompartmentModel();
    private static final String COMPARTMENT_ID_COLUMN = "id";
    private static final String COMPARTMENT_NAME_COLUMN = "compartmentName";
    private static final String POSITION_COLUMN = "position";
    private static final String STORAGE_ID_COLUMN = "storageId";
    private static final String SELECT_ALL_QUERY = "SELECT * FROM Compartments WHERE " +
            STORAGE_ID_COLUMN + "= ?";

    private static final String INSERT_QUERY = "INSERT INTO Compartments ("
            + COMPARTMENT_NAME_COLUMN + "," + POSITION_COLUMN + "," + STORAGE_ID_COLUMN + ") "
            + " VALUES(?,?,?)";

    private static final String SELECT_COMPARTMENT_QUERY = "SELECT * FROM Compartments WHERE "
            + COMPARTMENT_ID_COLUMN + " = ?";

    private static final String UPDATE_QUERY = "UPDATE Compartments SET "
            + COMPARTMENT_NAME_COLUMN + " = ?, "
            + POSITION_COLUMN + " = ?, "
            + STORAGE_ID_COLUMN + " = ? WHERE "
            + COMPARTMENT_ID_COLUMN + " = ?";

    private static final String DELETE_QUERY = "DELETE FROM Compartments WHERE "
            + COMPARTMENT_ID_COLUMN + " = ?";

    public static DbCompartmentModel getCompartmentModelInstance() {
        return compartmentModelInstance;
    }

    private DbCompartmentModel() {}

    public String setNumberOfElements(List<Integer> ids) {
        StringBuilder elements = new StringBuilder();

        for (int i = 0; i < ids.size(); i++) {
            if (i != ids.size() - 1) {
                elements.append("?, ");
            } else {
                elements.append("?");
            }
        }
        return elements.toString();
    }

    @Override
    public List<Compartment> getAllCompartmentsInStorage(int storageId) {
        List<Compartment> compartments = new ArrayList<>();
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_QUERY)
        ) {
            statement.setInt(1, storageId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Compartment.Builder builder = new Compartment.Builder();

                Compartment compartment = builder.setId(resultSet.getInt(COMPARTMENT_ID_COLUMN))
                        .setCompartmentName(resultSet.getString(COMPARTMENT_NAME_COLUMN))
                        .setPosition(resultSet.getString(POSITION_COLUMN))
                        .setStorageId(resultSet.getInt(STORAGE_ID_COLUMN))
                        .build();

                compartments.add(compartment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return compartments;
    }

    @Override
    public Compartment getCompartment(int compartmentId) {
        Compartment.Builder builder = new Compartment.Builder();
        Compartment compartment = null;
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement selectStatement = conn.prepareStatement(SELECT_COMPARTMENT_QUERY)
        ) {
            selectStatement.setInt(1, compartmentId);
            ResultSet resultSet = selectStatement.executeQuery();

            while (resultSet.next()) {
                compartment = builder.setId(resultSet.getInt(COMPARTMENT_ID_COLUMN))
                        .setCompartmentName(resultSet.getString(COMPARTMENT_NAME_COLUMN))
                        .setPosition(resultSet.getString(POSITION_COLUMN))
                        .setStorageId(resultSet.getInt(STORAGE_ID_COLUMN))
                        .build();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return compartment;
    }

    @Override
    public boolean addCompartment(Compartment compartment) {
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement insertQuery = conn.prepareStatement(INSERT_QUERY)
        ) {
            insertQuery.setString(1, compartment.getCompartmentName());
            insertQuery.setString(2, compartment.getPosition());
            insertQuery.setInt(3, compartment.getStorageId());
            return insertQuery.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean modifyCompartment(Compartment compartment) {
        try (Connection conn = DBConnection.getConnection();
             PreparedStatement modifyQuery = conn.prepareStatement(UPDATE_QUERY)
        ) {
            modifyQuery.setString(1, compartment.getCompartmentName());
            modifyQuery.setString(2, compartment.getPosition());
            modifyQuery.setInt(3, compartment.getStorageId());
            modifyQuery.setInt(4, compartment.getId());
            return modifyQuery.executeUpdate() != 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteCompartment(int id) {
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement deleteQuery = conn.prepareStatement(DELETE_QUERY)
        ) {
            deleteQuery.setInt(1, id);
            return deleteQuery.executeUpdate() != 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int deleteCompartments(List<Integer> ids) {
        String deleteQueryString = "DELETE FROM Compartments WHERE "
                + COMPARTMENT_ID_COLUMN + " IN(" + setNumberOfElements(ids) + ")";
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement deleteQuery = conn.prepareStatement(deleteQueryString)
        ) {
            for (int i = 0; i < ids.size(); i++) {
                deleteQuery.setInt(i + 1, ids.get(i));
            }
            return deleteQuery.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
