package storage.model;

import storage.data.ItemsInStorage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbItemsInStorageModel implements ItemsInStorageModel {

    private static final DbItemsInStorageModel itemsInStorageInstance = new DbItemsInStorageModel();
    private static final String ID_COLUMN = "id";
    private static final String ITEM_IN_STORAGE_NAME_COLUMN = "itemName";
    private static final String COMPARTMENT_ID_COLUMN = "compartmentId";
    private static final String DATE_ADDED_COLUMN = "dateAdded";
    private static final String DATE_OF_EXPIRATION_COLUMN = "dateOfExpiration";
    private static final String QUANTITY_COLUMN = "quantity";
    private static final String SELECT_ALL_QUERY = "SELECT * FROM ItemsInStorage WHERE " +
            COMPARTMENT_ID_COLUMN + " = ?";

    private static final String SELECT_ITEM_IN_STORAGE_QUERY =
            "SELECT * FROM ItemsInStorage WHERE "
                    + ID_COLUMN + " = ?";

    private static final String INSERT_QUERY = "INSERT INTO ItemsInStorage ("
            + COMPARTMENT_ID_COLUMN + ","
            + ITEM_IN_STORAGE_NAME_COLUMN + ","
            + DATE_ADDED_COLUMN + ","
            + DATE_OF_EXPIRATION_COLUMN + ","
            + QUANTITY_COLUMN + ")"
            + "VALUES(?,?,?,?,?)";

    private static final String UPDATE_QUERY = "UPDATE ItemsInStorage SET "
            + COMPARTMENT_ID_COLUMN + " = ?, "
            + ITEM_IN_STORAGE_NAME_COLUMN + " = ?, "
            + DATE_ADDED_COLUMN + " = ?, "
            + DATE_OF_EXPIRATION_COLUMN + " = ?, "
            + QUANTITY_COLUMN + " = ? WHERE "
            + ID_COLUMN + " = ?";

    private static final String DELETE_QUERY = "DELETE FROM ItemsInStorage WHERE "
            + ID_COLUMN + " = ?";

    public static DbItemsInStorageModel getItemsInStorageInstance() {
        return itemsInStorageInstance;
    }

    private DbItemsInStorageModel() {}

    public String setNumberOfElements(List<Integer> ids) {
        StringBuilder elements = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i != ids.size() - 1) {
                elements.append("?, ");
            } else {
                elements.append("?");
            }
        }
        return elements.toString();
    }

    @Override
    public List<ItemsInStorage> getAllItemsInStorage(int compartmentId) {
        List<ItemsInStorage> allItemsInStorage = new ArrayList<>();
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement preparedStatement = conn.prepareStatement(SELECT_ALL_QUERY)
        ) {
            preparedStatement.setInt(1, compartmentId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ItemsInStorage.Builder builder = new ItemsInStorage.Builder();
                ItemsInStorage itemsInStorage = builder.setId(resultSet.getInt(ID_COLUMN))
                        .setCompartmentId(resultSet.getInt(COMPARTMENT_ID_COLUMN))
                        .setName(resultSet.getString(ITEM_IN_STORAGE_NAME_COLUMN))
                        .setDateAdded(resultSet.getDate(DATE_ADDED_COLUMN).toLocalDate())
                        .setDateOfExpiration(resultSet.getDate(DATE_OF_EXPIRATION_COLUMN).toLocalDate())
                        .setQuantity(resultSet.getInt(QUANTITY_COLUMN))
                        .build();
                allItemsInStorage.add(itemsInStorage);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allItemsInStorage;
    }

    @Override
    public ItemsInStorage getItemInStorage(int itemId) {
        ItemsInStorage item = null;
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement selectItemStatement = conn.prepareStatement(SELECT_ITEM_IN_STORAGE_QUERY)
        ) {
            selectItemStatement.setInt(1, itemId);
            ResultSet resultSet = selectItemStatement.executeQuery();

            while (resultSet.next()) {
                ItemsInStorage.Builder builder = new ItemsInStorage.Builder();
                item = builder.setId(resultSet.getInt(ID_COLUMN))
                        .setCompartmentId(resultSet.getInt(COMPARTMENT_ID_COLUMN))
                        .setName(resultSet.getString(ITEM_IN_STORAGE_NAME_COLUMN))
                        .setDateAdded(resultSet.getDate(DATE_ADDED_COLUMN).toLocalDate())
                        .setDateOfExpiration(resultSet.getDate(DATE_OF_EXPIRATION_COLUMN).toLocalDate())
                        .setQuantity(resultSet.getInt(QUANTITY_COLUMN))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return item;
    }

    @Override
    public boolean addItemInStorage(ItemsInStorage itemsInStorage) {
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement insertQuery = conn.prepareStatement(INSERT_QUERY)
        ) {
            insertQuery.setInt(1, itemsInStorage.getCompartmentId());
            insertQuery.setString(2, itemsInStorage.getItemInStorageName());
            insertQuery.setDate(3, Date.valueOf(itemsInStorage.getDateAdded()));
            insertQuery.setDate(4, Date.valueOf(itemsInStorage.getDateOfExpiration()));
            insertQuery.setInt(5, itemsInStorage.getQuantity());

            return insertQuery.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean modifyItemInStorage(ItemsInStorage itemsInStorage) {
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement updateQuery = conn.prepareStatement(UPDATE_QUERY)
        ) {
            updateQuery.setInt(1, itemsInStorage.getCompartmentId());
            updateQuery.setString(2, itemsInStorage.getItemInStorageName());
            updateQuery.setDate(3, Date.valueOf(itemsInStorage.getDateAdded()));
            updateQuery.setDate(4, Date.valueOf(itemsInStorage.getDateOfExpiration()));
            updateQuery.setInt(5, itemsInStorage.getQuantity());
            updateQuery.setInt(6, itemsInStorage.getId());
            return updateQuery.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public boolean deleteItemInStorage(int id) {
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement deleteQuery = conn.prepareStatement(DELETE_QUERY)
        ) {
            deleteQuery.setInt(1, id);
            return deleteQuery.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int deleteItemsInStorage(List<Integer> ids) {
        String deleteQueryString = "DELETE FROM ItemsInStorage WHERE "
                + ID_COLUMN + " IN(" + setNumberOfElements(ids) + ")";
        try (
                Connection conn = DBConnection.getConnection();
                PreparedStatement deleteQuery = conn.prepareStatement(deleteQueryString)
        ) {
            for (int i = 0; i < ids.size(); i++) {
                deleteQuery.setInt(i + 1, ids.get(i));
            }
            return deleteQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}

