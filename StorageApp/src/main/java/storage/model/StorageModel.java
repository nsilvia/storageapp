package storage.model;

import storage.data.Storage;

import java.util.List;

public interface StorageModel {
    List<Storage> getAllStorages();
    Storage getStorage(int storageNumber);
    boolean addStorage(Storage storage);
    boolean modifyStorage(Storage storage);
    boolean deleteStorage(int id);
    int deleteStorages(List<Integer> id);
}
