package storage.controller;

import storage.data.ItemsInStorage;
import storage.model.DbItemsInStorageModel;

import java.util.List;

public class ItemsControllerImpl implements ItemsController {
    private final static ItemsControllerImpl itemsInStorageControllerInstance = new ItemsControllerImpl();

    private final DbItemsInStorageModel itemsInStorageModelInstance = DbItemsInStorageModel.getItemsInStorageInstance();

    public static ItemsControllerImpl getInstance() {
        return itemsInStorageControllerInstance;
    }

    private ItemsControllerImpl() {}


    @Override
    public List<ItemsInStorage> getAllItemsInStorage(int compartmentId) {
        return itemsInStorageModelInstance.getAllItemsInStorage(compartmentId);
    }

    @Override
    public ItemsInStorage getItemInStorage(int id) {
        return itemsInStorageModelInstance.getItemInStorage(id);
    }

    @Override
    public boolean addItemInStorage(ItemsInStorage itemInStorage) {
        return itemsInStorageModelInstance.addItemInStorage(itemInStorage);
    }

    @Override
    public boolean modifyItemInStorage(ItemsInStorage itemInStorage) {
        return itemsInStorageModelInstance.modifyItemInStorage(itemInStorage);
    }

    @Override
    public boolean deleteItemInStorage(int id) {
        return itemsInStorageModelInstance.deleteItemInStorage(id);
    }

    @Override
    public int deleteItemsInStorage(List<Integer> ids) {
        return itemsInStorageModelInstance.deleteItemsInStorage(ids);
    }
}
