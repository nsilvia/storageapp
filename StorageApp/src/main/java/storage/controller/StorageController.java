package storage.controller;

import storage.data.Storage;

import java.util.List;

public interface StorageController {
    Storage getStorage(int id);
    List<Storage> getAllStorages();
    boolean addStorage(Storage storage);
    boolean modifyStorage(Storage storage);
    boolean deleteStorage(int id);
    int deleteStorages(List<Integer> ids);
}
