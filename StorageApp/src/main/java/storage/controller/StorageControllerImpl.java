package storage.controller;

import storage.data.Storage;
import storage.model.DbStorageModel;

import java.util.List;

public class StorageControllerImpl implements StorageController {

    private final static StorageControllerImpl storageControllerInstance = new StorageControllerImpl();

    private final DbStorageModel storageModelInstance = DbStorageModel.getStorageModelInstance();

    public static StorageControllerImpl getInstance(){
        return storageControllerInstance;
    }

    private StorageControllerImpl(){}

    @Override
    public Storage getStorage(int id) {
        return storageModelInstance.getStorage(id);
    }

    @Override
    public List<Storage> getAllStorages() {
        return storageModelInstance.getAllStorages();
    }

    @Override
    public boolean addStorage(Storage storage) {
        return storageModelInstance.addStorage(storage);
    }

    @Override
    public boolean modifyStorage(Storage storage) {
        return storageModelInstance.modifyStorage(storage);
    }

    @Override
    public boolean deleteStorage(int id) {
        return storageModelInstance.deleteStorage(id);
    }

    @Override
    public int deleteStorages(List<Integer> ids) {
        return storageModelInstance.deleteStorages(ids);
    }

}
