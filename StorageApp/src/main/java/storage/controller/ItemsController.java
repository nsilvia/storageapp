package storage.controller;

import storage.data.ItemsInStorage;

import java.util.List;

public interface ItemsController {
    List<ItemsInStorage> getAllItemsInStorage(int compartmentId);
    ItemsInStorage getItemInStorage(int id);
    boolean addItemInStorage(ItemsInStorage itemInStorage);
    boolean modifyItemInStorage(ItemsInStorage itemInStorage);
    boolean deleteItemInStorage(int id);
    int deleteItemsInStorage(List<Integer> ids);
}
