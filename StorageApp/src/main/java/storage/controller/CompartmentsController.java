package storage.controller;

import storage.data.Compartment;

import java.util.List;

public interface CompartmentsController {
    List<Compartment> getAllCompartmentsInStorage(int storageId);
    Compartment getCompartment(int compartmentId);
    boolean addCompartment(Compartment compartment);
    boolean modifyCompartment(Compartment compartment);
    boolean deleteCompartment(int id);
    int deleteCompartments(List<Integer> id);
}
