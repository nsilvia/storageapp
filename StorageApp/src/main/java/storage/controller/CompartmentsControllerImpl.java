package storage.controller;

import storage.data.Compartment;
import storage.model.DbCompartmentModel;

import java.util.List;

public class CompartmentsControllerImpl implements CompartmentsController {

    private static final CompartmentsControllerImpl CompartmentsControllerImplInstance = new CompartmentsControllerImpl();

    private final DbCompartmentModel compartmentModelInstance = DbCompartmentModel.getCompartmentModelInstance();

    public static CompartmentsControllerImpl getInstance() {
        return CompartmentsControllerImplInstance;
    }

    private CompartmentsControllerImpl() {
    }

    @Override
    public List<Compartment> getAllCompartmentsInStorage(int storageId) {
        return compartmentModelInstance.getAllCompartmentsInStorage(storageId);
    }

    @Override
    public Compartment getCompartment(int compartmentId) {
        return compartmentModelInstance.getCompartment(compartmentId);
    }

    @Override
    public boolean addCompartment(Compartment compartment) {
        return compartmentModelInstance.addCompartment(compartment);
    }

    @Override
    public boolean modifyCompartment(Compartment compartment) {
        return compartmentModelInstance.modifyCompartment(compartment);
    }

    @Override
    public boolean deleteCompartment(int id) {
        return compartmentModelInstance.deleteCompartment(id);
    }

    @Override
    public int deleteCompartments(List<Integer> id) {
        return compartmentModelInstance.deleteCompartments(id);
    }
}
