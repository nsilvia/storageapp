package storage.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    private String userName = "Silvia";
    private String password = "test1234";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (isAuthenticated(servletRequest)){
            filterChain.doFilter(servletRequest, servletResponse);
        } else{
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private boolean isAuthenticated(ServletRequest req){
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");

        return this.userName.equals(userName) && this.password.equals(password);

    }
}
